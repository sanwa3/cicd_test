# Cicd Test

```sh
$ go run main.go 
2024-04-17T20:32:44+08:00 INFO   cors/gorestful/cors.go:52 > cors enabled component:CORS
2024-04-17T20:32:44+08:00 INFO   metric/restful/metric.go:58 > Get the Metric using http://127.0.0.1:8080/metrics component:METRIC
2024-04-17T20:32:44+08:00 INFO   health/restful/check.go:62 > Get the Health using http://127.0.0.1:8080/healthz component:HEALTH_CHECK
2024-04-17T20:32:44+08:00 INFO   apidoc/restful/swagger.go:58 > Get the API Doc using http://127.0.0.1:8080/apidocs.json component:API_DOC
2024-04-17T20:32:44+08:00 INFO   ioc/server/server.go:74 > loaded configs: [app.v1 trace.v1 log.v1 gorestful_webframework.v1 go_restful_cors.v1 grpc.v1 http.v1] component:SERVER
2024-04-17T20:32:44+08:00 INFO   ioc/server/server.go:75 > loaded controllers: [] component:SERVER
2024-04-17T20:32:44+08:00 INFO   ioc/server/server.go:76 > loaded apis: [metric.v1 health.v1 hello_module.v1 apidoc.v1] component:SERVER
2024-04-17T20:32:44+08:00 INFO   config/http/http.go:142 > HTTP服务启动成功, 监听地址: 127.0.0.1:8080 component:HTTP
2024-04-17T20:32:44+08:00 ERROR  config/http/http.go:144 > listen tcp 127.0.0.1:8080 component:HTTP
```

## 容器构建

```sh
docker build -t test_api -f Dockerfile .
```


